package com.example.tommy.testreg;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


public class MainActivity extends ActionBarActivity {

//    private TextView scrollResponse = null;
    private Spinner schoolSpinner = null;
    private Spinner departmentSpinner = null;
    private Spinner courseSpinner = null;
    private NumberPicker sectionPicker = null;
    private WeekView weekView = null;

    private JSONArray schoolsJSON = null;
    private JSONArray departmentsJSON = null;
    private JSONArray coursesJSON = null;
    private JSONArray sectionsJSON = null;

    ArrayAdapter<String> itemList = null;
    ArrayAdapter<String> departmentList = null;
    ArrayAdapter<String> courseList = null;

    private String termCode = "20151";

    private String baseUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        baseUrl = getResources().getString(R.string.base_url);

        schoolSpinner = (Spinner) findViewById(R.id.spinner);
        departmentSpinner = (Spinner) findViewById(R.id.spinner2);
        courseSpinner = (Spinner) findViewById(R.id.spinner3);
        sectionPicker = (NumberPicker) findViewById(R.id.numberPicker);
//        scrollResponse = (TextView) findViewById(R.id.textView4);
        weekView = (WeekView) findViewById(R.id.weekView);

         itemList= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        departmentList = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        courseList = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);

        schoolSpinner.setAdapter(itemList);
        departmentSpinner.setAdapter(departmentList);
        courseSpinner.setAdapter(courseList);

        sectionPicker.setMinValue(0);
        sectionPicker.setMaxValue(2);

        sectionPicker.setValue(0);
        sectionPicker.setDisplayedValues(new String[]{"Section 1", "Section 2", "Section 3"});
        sectionPicker.setWrapSelectorWheel(false);

        weekView.setMonthChangeListener(new WeekView.MonthChangeListener() {
            @Override
            public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {
                // Populate the week view with some events.
                List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();

                return events;
            }
        });

        weekView.setOnEventClickListener(new WeekView.EventClickListener() {
            @Override
            public void onEventClick(com.alamkanak.weekview.WeekViewEvent weekViewEvent, android.graphics.RectF rectF) {

            }

        });

        weekView.setEventLongPressListener(new WeekView.EventLongPressListener(){
            @Override
            public void onEventLongPress(com.alamkanak.weekview.WeekViewEvent weekViewEvent, android.graphics.RectF rectF)
            {
            }
        });

        sectionPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                displayTimes(newVal);
            }
        });

        schoolSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                JSONObject schoolObject = null;

                try {
                    schoolObject = schoolsJSON.getJSONObject(position);
                } catch (JSONException e) {
                    Toast.makeText(getBaseContext(), "JSON array screwed up finding school", Toast.LENGTH_LONG).show();
                    return;
                }

                String schoolCode = null;

                try {
                    schoolCode = schoolObject.getString("SOC_SCHOOL_CODE");
                } catch (JSONException e) {
                    Toast.makeText(getBaseContext(), "JSON array screwed up finding school code", Toast.LENGTH_LONG).show();
                    return;
                }

                new RetrieveDepartmentsTask().execute(schoolCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        departmentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                JSONObject depObject = null;

                try {
                    depObject = departmentsJSON.getJSONObject(position);
                } catch (JSONException e) {
                    Toast.makeText(getBaseContext(), "JSON array screwed up finding school", Toast.LENGTH_LONG).show();
                    return;
                }

                String depCode = null;

                try {
                    depCode = depObject.getString("SOC_DEPARTMENT_CODE");
                } catch (JSONException e) {
                    Toast.makeText(getBaseContext(), "JSON array screwed up finding school code", Toast.LENGTH_LONG).show();
                    return;
                }

                new RetrieveCoursesTask().execute(depCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        courseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                JSONObject classObject = null;

                try {
                    classObject = coursesJSON.getJSONObject(position);
                } catch (JSONException e) {
                    Toast.makeText(getBaseContext(), "JSON array screwed up finding school", Toast.LENGTH_LONG).show();
                    return;
                }

                String courseCode = null;

                try {
                    courseCode = classObject.getString("COURSE_ID");
                } catch (JSONException e) {
                    Toast.makeText(getBaseContext(), "JSON array screwed up finding school code", Toast.LENGTH_LONG).show();
                    return;
                }

                new RetrieveSectionsTask().execute(courseCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        new RetrieveSchoolsTask().execute("http://petri.esd.usc.edu/socAPI/Schools/");
    }

    private String getEventTitle(Calendar time) {
        return String.format("Event of %02d:%02d %s/%d", time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH)+1, time.get(Calendar.DAY_OF_MONTH));
    }

    private void displayTimes(int position)
    {
        JSONObject sectionJSON = null;

        try{
            sectionJSON = sectionsJSON.getJSONObject(position);
        }
        catch(JSONException e)
        {
            Toast.makeText(getBaseContext(), "JSON array screwed up finding section", Toast.LENGTH_LONG).show();
            return;
        }
        String beginTime = null;
        String endTime = null;

        try {
            beginTime = sectionJSON.getString("BEGIN_TIME");
            endTime = sectionJSON.getString("END_TIME");
        }
        catch(JSONException e)
        {
            Toast.makeText(getBaseContext(), "JSON screwed up finding times", Toast.LENGTH_LONG).show();
            return;
        }

//        scrollResponse.setText(beginTime + "\n" + endTime);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.z
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public String GET(String url){
        InputStream inputStream = null;
        String result = "";

        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            Log.i("GET", url);

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private class RetrieveDepartmentsTask extends AsyncTask<String, Void, String>  {
        protected String doInBackground(String... schoolCode) {

            return GET(baseUrl + "Schools/" + schoolCode[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            JSONObject schoolObject = null;

            Log.i("RESULT", result);

            departmentList.clear();

            try {
                schoolObject = new JSONArray(result).getJSONObject(0);
            }
            catch(org.json.JSONException e) {
                Toast.makeText(getBaseContext(), "JSON object screwed up", Toast.LENGTH_LONG).show();
                return;
            }

            try{
                departmentsJSON = schoolObject.getJSONArray("SOC_DEPARTMENT_CODE");
            }
            catch(org.json.JSONException e) {
                Toast.makeText(getBaseContext(), "JSON object screwed up", Toast.LENGTH_LONG).show();
                return;
            }

            for(int i = 0; i < departmentsJSON.length(); i++)
            {
                try {
                    JSONObject departmentObject = departmentsJSON.getJSONObject(i);

                    String schoolName = departmentObject.getString("SOC_DEPARTMENT_CODE");

                    departmentList.add(schoolName);
                }
                catch(org.json.JSONException e)
                {
                    Toast.makeText(getBaseContext(), "JSON request screwed up", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            departmentList.notifyDataSetChanged();
        }
    }

    private class RetrieveSchoolsTask extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {

            return GET(baseUrl + "Schools/");
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                schoolsJSON = new JSONArray(result);
            } catch (org.json.JSONException e) {
                Toast.makeText(getBaseContext(), "JSON array screwed up", Toast.LENGTH_LONG).show();
                return;
            }

            for (int i = 0; i < schoolsJSON.length(); i++) {
                try {
                    JSONObject schoolObj = schoolsJSON.getJSONObject(i);

                    String schoolName = schoolObj.getString("SOC_SCHOOL_DESCRIPTION");

                    itemList.add(schoolName);
                } catch (org.json.JSONException e) {
                    Toast.makeText(getBaseContext(), "JSON request screwed up", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            itemList.notifyDataSetChanged();
        }
    }

    private class RetrieveCoursesTask extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... depCode) {

            return GET(baseUrl + "Courses/" + termCode + "/" + depCode[0] + "/");
        }

        @Override
        protected void onPostExecute(String result) {

            courseList.clear();

            try {
                coursesJSON = new JSONArray(result);
            } catch (org.json.JSONException e) {
                Toast.makeText(getBaseContext(), "JSON array screwed up", Toast.LENGTH_LONG).show();
                return;
            }

            for (int i = 0; i < coursesJSON.length(); i++) {
                try {
                    JSONObject courseObj = coursesJSON.getJSONObject(i);

                    String courseTitle = courseObj.getString("TITLE");

                    courseList.add(courseTitle);
                } catch (org.json.JSONException e) {
                    Toast.makeText(getBaseContext(), "JSON request screwed up", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            courseList.notifyDataSetChanged();
        }
    }

    private class RetrieveSectionsTask extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... courseCode) {

            return GET(baseUrl + "Courses/" + termCode + "/" + courseCode[0] + "/");
        }

        @Override
        protected void onPostExecute(String result) {
            JSONObject courseJSON = null;
            try {
                courseJSON = new JSONObject(result);
            } catch (JSONException e)
            {
                Toast.makeText(getBaseContext(), "JSON object screwed up", Toast.LENGTH_LONG).show();
                return;
            }

            try {
                sectionsJSON = courseJSON.getJSONArray("V_SOC_SECTION");
            } catch (org.json.JSONException e) {
                Toast.makeText(getBaseContext(), "JSON array screwed up", Toast.LENGTH_LONG).show();
                return;
            }

            String[] sectionStrings = new String[sectionsJSON.length()];
            for (int i = 0; i < sectionsJSON.length(); i++) {
                try {
                    JSONObject sectionObj = sectionsJSON.getJSONObject(i);

                    String courseTitle = sectionObj.getString("SECTION_ID");

                    sectionStrings[i] = courseTitle;
                } catch (org.json.JSONException e) {
                    Toast.makeText(getBaseContext(), "JSON request screwed up", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            sectionPicker.setDisplayedValues(null);
            sectionPicker.setValue(0);
            sectionPicker.setMaxValue(sectionStrings.length - 1);
            sectionPicker.setDisplayedValues(sectionStrings);
            sectionPicker.setWrapSelectorWheel(false);
            displayTimes(0);
        }
    }


}


